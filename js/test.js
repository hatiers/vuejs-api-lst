var app = new Vue({
  el: '#app',
  mounted: function() {
    this.init()
  },
  methods: {
    init: function() {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', this.dfsmUrl + "/api/domains?")
      xhr.onload = function() {
        if (xhr.status === 200) {
          let data = JSON.parse(xhr.responseText)
          app.domains = JSON.parse(JSON.stringify(data['hydra:member']))
        }
      }
      xhr.send();
    },
    search: function() {
      let url = "/api/entries?"
      url += (app.request.string) ? "cleanLemma=" + app.request.string : ""
      url += (app.request.valid != "") ? "&valid=" + app.request.valid : ""
      url += (app.request.def != "") ? "&senses.def=" + app.request.def : ""


      app.request.domain.forEach(function(item) {
        url += (app.request.domain != "") ? "&senses.domain[]=" + item : ""
      });

      this.requestApi(url)
    },

    nama: function() {
      let url = "/api/entries?"
      url += "senses.domain=" + app.domainNama
      url += "&exists[senses.notes.xrs]=0"
      this.requestApi(url)
    },

    goTo(e) {
      let url = e.target.getAttribute("data-url")
      this.requestApi(url)
    },
    requestApi(url) {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', app.dfsmUrl + url)
      xhr.onload = function() {
        if (xhr.status === 200) {
          let data = JSON.parse(xhr.responseText)
          app.results = JSON.parse(JSON.stringify(data))
        }
      }
      xhr.send();
    }
  },
  data: {
    request: {
      string: "",
      valid: "",
      def: "",
      domain: [""]
    },
    dfsmUrl: "http://localhost:276",
    domains: null,
    results: null,
    domainNama : 5,
    namas: null
  }
})
